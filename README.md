**TITLE: BLOOD LINKS**


**ABSTARCT:**
Blood is an important component of Human Body.
Blood plays an important role in regulating the body's systems and maintaining homeostasis.


**INTRODUCTION:**
The need for Blood is paramount as the increase in the number of accidents is observed.
We see many posts and messages regarding the desperate need of blood in case of emergency.
It’s crucial for the Blood to be available in time to save a life.
We intend to make a fully functional Android Application which can provide the information about the available Blood Donors in the required locality.
We also provide the chance for a socially responsible person to become a Blood Donor by Volunteering.
And we urge the users to use this app responsibly and not to misuse the application to violate donor's privacy.

**PURPOSE:**
People in Need can search for the Potential Donors by giving the Blood Group of the Patient and required Locality. A list of Donors with their Contact Numbers will be displayed.
They can contact the Potential Donors and on Mutual Understanding... Lives can be SAVED.
A blood donation occurs when a person voluntarily has blood drawn and used for transfusions and made into biopharmaceutical medications. Most blood donors are unpaid volunteers who donate blood for a community supply.
Potential. Donors are evaluated for anything that might make their blood unsafe to use. The screening includes testing for diseases that can be transmitted by a blood transfusion.

**PROCESSING:**
We have collected datasets based on different blood groups from different states.We have redefined the dataset and eliminated false information about the donors.
Based on blood group, the information about the donor will be available.

**AIMS AND OBJECTIVES:**

Creating an platform for Blood Donors and Recipients.
To conceptualize and develop a user interface and design.

**ADVANTAGES:**

* Helps Blood Banks to automate blood donor and depository online.
* Encourages blood donors to donate.
* Helps people find blood donors in times of need.


**SCOPE:**

* This application is linked to the internet, so everyone can use the data.
* This Application can be expanded with availability worldwide.
* Providing donor an option of change his/her availability.


** libary Used : **
- volley : 'com.android.volley:volley:1.1.1'

**Directory Structure:**
- java main folder  
> - PlaceholderFragement : Contains all the file fragment related to search and volunteer.
> - JSONAdapter : Used to for the listView converts the JSONArray to JSONObject and then get and set the key to the widgets in the app.
> - CustomRequest : Used to parse the request in the case of String. 
> - SectionPageAdapter : Holds the name of the tab layout and tabView related functionality.
> -BloodDetails : This is another activity with some data to show to the user .
> -MainActivity : This is the main activity.


**NOTE:**
- Don't remove handleSSLHandshake() in MainActivity
- Keep android:usesCleartextTraffic="true"
- Don't Delete CustomRequest.class
- Don't remove newtwork_security_config.xml

**NEXT BUILD:**
- improvement in validation while inserting 
- improvement in performance 



**TEAM:**

*Nayan Raj Adhikary and Sameer Kousik Kasivajhula*

