package com.swecha.bloodlinks.ui.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.swecha.bloodlinks.BloodDetails;
import com.swecha.bloodlinks.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class PlaceholderFragment extends Fragment {
    private ListView ls;
    private SearchView sv;
    private Spinner spinner;
    private Button volunteer;
    private EditText nameEd,bloodGroupEd,AddressEd,AgeEd,phoneNumberEd;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private PageViewModel pageViewModel;




    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        View root1 = inflater.inflate(R.layout.fragment_volunteer,container,false);
        int index = getArguments().getInt(ARG_SECTION_NUMBER);
        Log.d("VIEW","Msg :"+index);
        if(index == 1){
            sv = root.findViewById(R.id.searchView);
            ls = root.findViewById(R.id.listView);
            spinner = root.findViewById(R.id.spinner);
            Set<String> set = new TreeSet<>();
//            TODO : set.add(list of bloodGroups from the database)'
            set.add("A+");
            set.add("A-");
            set.add("AB+");
            set.add("AB-");
            set.add("B+");
            set.add("B-");
            set.add("O+");
            set.add("O-");
            List<String> spinnerLs = new ArrayList<>(set);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerLs);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
//            Log.d("VAL", "onCreateView: "+val);
            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
//                    TODO : make a query to search as String
                    String blood = String.valueOf(spinner.getSelectedItem());
                    try {
                        resquested(blood,query);

                    }catch (JSONException e){
                        Toast.makeText(getContext(),"Error While parsing",Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });

            return root;
        }else {
            volunteer = root1.findViewById(R.id.vSubmit);
            nameEd = root1.findViewById(R.id.nametv);
            AddressEd = root1.findViewById(R.id.atv);
            bloodGroupEd = root1.findViewById(R.id.bloodGrouptv);
            AgeEd = root1.findViewById(R.id.agetv);
            phoneNumberEd = root1.findViewById(R.id.phonenumbertv);
            volunteer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    TODO valid for real data insertion after validation insert into database
                        String nameFetch = nameEd.getText().toString().trim();
                        String addressFetch = AddressEd.getText().toString().trim();
                        String bloodFetch = bloodGroupEd.getText().toString().trim();
                        String ageFetch = AgeEd.getText().toString().trim();
                        String phoneFetch = phoneNumberEd.getText().toString().trim();
                        Log.d("VALUE","MSG :"+nameFetch+" "+addressFetch+" "+bloodFetch+" "+ageFetch+" "+phoneFetch);
//                       int counter = 0;
                    // TODO : Make Validation for empty fields and check if the number has 10 digit
                    if(nameFetch.isEmpty() || addressFetch.isEmpty() || bloodFetch.isEmpty() || ageFetch.isEmpty() || phoneFetch.length() != 10){
                        Toast.makeText(getContext(),"Please Enter Valid Data",Toast.LENGTH_LONG).show();
                    }else{
                        try {
                            requestInsert(nameFetch, addressFetch, bloodFetch, ageFetch, phoneFetch);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
            });
            return root1;
        }

    }

    private void requestInsert(final String name, final String address, final String blood,final String age, final String phone) throws JSONException {
        RequestQueue requestQueue;
        final String TAG = "Requested Function";
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(),1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        final StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                getResources().getString(R.string.insert_url),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Success"+response);
                        Toast.makeText(getContext(),"ThankYou for Volunteering !!",Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
//                Log.e(TAG, "Success");
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Blood_Group",blood );
                params.put("Location", address );
                params.put("name",name );
                params.put("age",age );
                params.put("Contact_Number", phone);
                return params;
            }
        };
        requestQueue.add(jsonObjRequest);

    }


    private void resquested(final String blood, final String location)throws JSONException{
        RequestQueue requestQueue;
        final String TAG = "Requested Function";
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(),1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        final StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                getResources().getString(R.string.base_url),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Success"+response);

                        try {
                            final JSONArray jsonArray = new JSONArray(response);
                            final JSONAdapter jsonAdapter = new JSONAdapter(getActivity(),jsonArray);
                            ls.setAdapter(jsonAdapter);
                            AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent intent = new Intent(getContext(),BloodDetails.class);
//                                    Toast.makeText(getContext(),"clicked"+jsonAdapter.getItem(position),Toast.LENGTH_SHORT).show();
                                    try {
                                        JSONObject innerrequest = (JSONObject) jsonAdapter.getItem(position);
                                        String name  = innerrequest.getString("name");
                                        String age = innerrequest.getString("age");
                                        String Blood_group  = innerrequest.getString("Blood_Group");
                                        String Address = innerrequest.getString("Address");
                                        String phoneNumber = innerrequest.getString("Contact_Number");
                                        intent.putExtra("name",name);
                                        intent.putExtra("age",age);
                                        intent.putExtra("bloodGroup",Blood_group);
                                        intent.putExtra("address",Address);
                                        intent.putExtra("phoneNumber",phoneNumber);
                                        startActivity(intent);
                                        Log.d("INNER REQUEST","THIS IS VALUE "+innerrequest);
                                    }catch (Exception e){e.printStackTrace();}
                                }
                            };
                            ls.setOnItemClickListener(listener);
                        }catch (Exception e){
                            Toast.makeText(getContext(),"Sorry!! No donors",Toast.LENGTH_SHORT).show();
//                            Toast.makeText(getContext(),"ERROR"+e,Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
                Log.e(TAG, "Success");
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Blood_Group", blood);
                params.put("Location", location);
                return params;
            }
        };
        requestQueue.add(jsonObjRequest);

    }

}