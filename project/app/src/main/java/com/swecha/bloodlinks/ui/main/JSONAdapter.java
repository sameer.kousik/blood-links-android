package com.swecha.bloodlinks.ui.main;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.swecha.bloodlinks.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONAdapter extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;
   private String namereal;

    public String getName() {
        return namereal;
    }

    public void setName(String namereal) {
        this.namereal = namereal;
    }

    public JSONAdapter(Activity activity, JSONArray jsonArray){
        this.activity = activity;
        this.jsonArray = jsonArray;
    }

    @Override
    public int getCount() {
        if(jsonArray == null)
            return 0;
        else
            return  jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        if(null==jsonArray) return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = (JSONObject) getItem(position);
        return jsonObject.optLong("id");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = activity.getLayoutInflater().inflate(R.layout.listitem,null);
        TextView name = convertView.findViewById(R.id.nameLv);
        TextView blood = convertView.findViewById(R.id.bloodLv);
        JSONObject jsonObject = (JSONObject) getItem(position);
        if(jsonObject != null){
            try {
                setName(jsonObject.getString("name"));
                String bloodj = jsonObject.getString("Blood_Group");
                name.setText(namereal);blood.setText(bloodj);
            }catch (Exception e){
                Log.d("FromAdapter","Error"+e);
            }
        }
        return convertView;
    }


}
