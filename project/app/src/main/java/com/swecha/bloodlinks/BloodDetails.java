package com.swecha.bloodlinks;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class BloodDetails extends AppCompatActivity {
    TextView nameVal, ageValue, bloodValue, addressValue, phoneValue;
    FloatingActionButton call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_details);
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String age = intent.getStringExtra("age");
        String bloodGroup = intent.getStringExtra("bloodGroup");
        String address = intent.getStringExtra("address");
        final String phoneNumber = intent.getStringExtra("phoneNumber");
//        Toast.makeText(this,"name :"+name+"age :"+age+"bloodGroup :"+bloodGroup+" Address :"+address+" phone :"+phoneNumber,Toast.LENGTH_SHORT).show();
        nameVal = findViewById(R.id.nameValue);
        ageValue = findViewById(R.id.ageValue);
        bloodValue = findViewById(R.id.bloodValue);
        addressValue = findViewById(R.id.locationValue);
        phoneValue = findViewById(R.id.phoneValue);
        call = findViewById(R.id.floatingActionButton);
        nameVal.setText(name);
        ageValue.setText(age);
        bloodValue.setText(bloodGroup);
        addressValue.setText(address);
        phoneValue.setText(phoneNumber);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri u = Uri.parse("tel:" +phoneNumber);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                try
                {
                    startActivity(i);
                }
                catch (SecurityException s)
                {
                    Toast.makeText(getApplication(), "Couldn't open dailer", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }
}
